/*
  ########################################################################################################

  Concatenate: building a supermatrix of characters by concatenating a set of multiple sequence alignment
  
  Copyright (C) 2018-2020  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.util.*;

public class Concatenate {

    // constants
    final static String VERSION = "0.1b.201024ac";
    final static String MISSING = "????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????"; // one thousand unknown character states
    final static String BLANK =   "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "; // one thousand blank characters
    final static String NO_FILE = "n.o.f.i.l.e";

    // options
    static File datafile;  // -d
    static File outfile;   // -o
    static boolean fasta;  // -f
    static boolean phylip; // -p
    static int length;     // -l

    // io 
    static BufferedReader in;
    static BufferedWriter out;

    // data
    static int k, n, l;
    static ArrayList<File> infile;
    static ArrayList<String> label;
    static ArrayList<StringBuilder> sm;

    // stuffs
    static int i, o, m, ii, nn, ll;
    static String line, disp;
    static File f;
    static StringBuilder seq;
    static ArrayList<String> lbl;
    static ArrayList<StringBuilder> ali;

    public static void main(String[] args) throws IOException {

	// ###########################
	// ##### doc             #####
	// ###########################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" Concatenate v." + VERSION + "                      Copyright (C) 2019-2020  Institut Pasteur");
	    System.out.println("");
	    System.out.println(" USAGE:   Concatenate  [-i <infile>]  [-o <outfile>]  [-f]  [-l <integer>]");
	    System.out.println("");
	    System.out.println(" where options are:");
	    System.out.println("");
	    System.out.println(" -i <infile>   to indicate the input file that contains containing every MSA file name (one");
	    System.out.println("               per line). If the  character % is set before a file name,  the corresponding");
	    System.out.println("               MSA will be not used to  build the supermatrix of characters.  Each MSA file");
	    System.out.println("               should contain one MSA  (DNA, RNA,  amino acid, O1, ...)  in either FASTA or");
	    System.out.println("               PHYLIP format. Relative/absolute path is allowed for each MSA file (default:");
	    System.out.println("               datafiles.txt).");
	    System.out.println(" -o <outfile>  To set the output file name (default: supermatrix.phy or supermatrix.fasta)");
	    System.out.println(" -f            To write the supermatrix of characters in FASTA format (default: PHYLIP)");
	    System.out.println(" -l <integer>  To set the maximum length of the written sequences (default: infinity); when ");
	    System.out.println("               option -f is not set, this option leads to a PHYLIP-interleaved ouput file ");
	    System.out.println("");
	    System.exit(0);
	}
	
	// ###########################
	// ##### reading options #####
	// ###########################
	datafile = new File("datafiles.txt"); if ( ! datafile.exists() ) datafile = new File(NO_FILE); 
	outfile = new File(NO_FILE);
	fasta = false;
	phylip = true;
	length = Integer.MAX_VALUE;
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") ) {
		datafile = new File(args[++o]); 
		if ( ! datafile.exists() ) { System.out.println(" " + datafile.toString() + " does not exist (option -i)"); System.exit(1); }
		continue;
	    }
	    if ( args[o].equals("-o") ) { outfile = new File(args[++o]); continue; }
	    if ( args[o].equals("-f") ) { fasta = true; phylip = false; continue; }
	    if ( args[o].equals("-p") ) { fasta = false; phylip = true; continue; }
	    if ( args[o].equals("-l") ) { 
		try { length = Integer.parseInt(args[++o]); } catch ( NumberFormatException e ) { System.out.println(" incorrect number format (option -l)"); System.exit(1); }
		if ( length < 0 ) { System.out.println(" incorrect number format (option -l)"); System.exit(1); }
	    }
	}
	if ( datafile.toString().equals(NO_FILE) ) { System.out.println(" no input file (option -i required)"); System.exit(0); }
	if ( outfile.toString().equals(NO_FILE) ) { if ( fasta ) outfile = new File("supermatrix.fasta"); else outfile = new File("supermatrix.phy"); }
	
	// #############################
	// ##### reading datafiles #####
	// #############################
	System.out.print(" Reading data ... ");
	infile = new ArrayList<File>();
	in = new BufferedReader(new FileReader(datafile));
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( (line.length() == 0) || line.startsWith("%") ) continue;
	    if ( ! (f=new File(line)).exists() ) { System.out.println("  file " + line + " does not exist"); System.exit(0); }
	    infile.add(f);
	}
	k = infile.size();
	System.out.println("[ok]");

	// #############################
	// ### building supermatrix  ###
	// #############################
	System.out.print(" Building supermatrix ");
	label = new ArrayList<String>(); sm = new ArrayList<StringBuilder>(); n = 0; l = 0;
	m = -1;
	while ( ++m < k ) {
	    disp = "(" + n + " taxa, " + l + " characters)"; System.out.print(disp);
	    // ### reading alignment m ###
	    line = "@"; in = new BufferedReader(new FileReader(infile.get(m)));
	    try { while ( (line=in.readLine().trim()).length() == 0 ) {} } catch ( NullPointerException e ) { in.close(); System.out.println(" " + infile.get(m) + " is empty"); System.exit(1); }
	    in.close(); lbl = new ArrayList<String>(); ali = new ArrayList<StringBuilder>();
	    // fasta format
	    if ( line.startsWith(">") ) { 
		in = new BufferedReader(new FileReader(infile.get(m)));
		seq = new StringBuilder(""); 
		while ( true ) {
		    try { if ( (line=in.readLine().trim()).length() == 0 ) continue; } catch ( NullPointerException e ) { ali.add(new StringBuilder(seq.toString())); in.close(); break; }
		    if ( line.startsWith(">") ) {
			if ( seq.length() != 0 ) { ali.add(new StringBuilder(seq.toString())); seq = new StringBuilder(""); }
			line = line.substring(1).trim(); lbl.add(line); continue;
		    }
		    seq = seq.append(line);
		}
	    }
	    // phylip format (sequential or interleaved)
	    else {
		in = new BufferedReader(new FileReader(infile.get(m)));
		while ( (line=in.readLine().trim()).length() == 0 ) {}
		o = line.indexOf(" "); nn = Integer.parseInt(line.substring(0,o)); ll = Integer.parseInt(line.substring(o).trim());
		ii = -1;
		while ( ++ii < nn ) { line = in.readLine().trim(); o = line.indexOf(" "); lbl.add(line.substring(0,o)); ali.add(new StringBuilder(line.substring(o).replaceAll(" ", ""))); }
		while ( ali.get(0).length() < ll ) {
		    while ( (line=in.readLine().trim()).length() == 0 ) {}
		    ii = 0; ali.set(ii, ali.get(ii).append(line.replaceAll(" ", ""))); while ( ++ii < nn ) ali.set(ii, ali.get(ii).append(in.readLine().replaceAll(" ", "")));
		}
		in.close();
	    }
	    // ### concatenating alignment m ###
	    nn = lbl.size(); ii = -1;
	    while ( ++ii < nn ) {
		if ( (i=label.indexOf((line=lbl.get(ii)))) == -1 ) { 
		    label.add(line); i = n; ++n; sm.add(new StringBuilder("")); 
		    ll = l; while ( ll > MISSING.length() ) { sm.set(i, sm.get(i).append(MISSING)); ll -= MISSING.length(); } sm.set(i, sm.get(i).append(MISSING.substring(0,ll)));
		}
		sm.set(i, sm.get(i).append(ali.get(ii).toString())); 
	    }
	    l = sm.get(label.indexOf(lbl.get(0))).length();
	    // ### filling holes ###
	    i = n;
	    while ( --i >= 0 ) 
		if ( (ll=sm.get(i).length()) < l ) {
		    ll = l - ll; while ( ll > MISSING.length() ) { sm.set(i, sm.get(i).append(MISSING)); ll -= MISSING.length(); }
		    sm.set(i, sm.get(i).append(MISSING.substring(0,ll)));
		}
	    o = disp.length(); while ( --o >= 0 ) System.out.print("\b\b "); 
	}
	System.out.println("(" + n + " taxa, " + l + " characters) ... [ok]");	
		    
	// #############################
	// ### writing supermatrix   ###
	// #############################
	System.out.println(" Supermatrix written into file " + outfile.toString());
	out = new BufferedWriter(new FileWriter(outfile));
	if ( fasta ) {
	    i = -1;
	    while ( ++i < n ) {
		out.write(">" + label.get(i)); out.newLine();
		ll = 0; while ( ll + length < l ) { out.write(sm.get(i).substring(ll , (ll+=length))); out.newLine(); } out.write(sm.get(i).substring(ll)); out.newLine();
	    }
	    out.close(); System.exit(0);
	}
	if ( phylip ) {
	    o = -1; i = n; while ( --i >= 0 ) o = ( (nn=label.get(i).length()) > o ) ? nn : o; ++o;
	    out.write(" " + n + " " + l); out.newLine();
	    ll = 0; 
	    while ( ll + length < l ) {
		i = -1;
		while ( ++i < n ) {
		    if ( ll == 0 ) out.write( (label.get(i) + BLANK).substring(0, o) );
		    out.write(sm.get(i).substring(ll, ll+length)); out.newLine();
		}
		out.newLine(); ll += length;
	    }
	    i = -1;
	    while ( ++i < n ) {
		if ( ll == 0 ) out.write( (label.get(i) + BLANK).substring(0, o) );
		out.write(sm.get(i).substring(ll)); out.newLine();
	    }
	    out.close();
	}
		
    }

}
