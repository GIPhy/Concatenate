# Concatenate

_Concatenate_ is a command line program written in [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html) to build a supermatrix of characters from a set of multiple sequence alignments (MSA) in FASTA or PHYLIP format.

## Compilation and execution

The source code of _Concatenate_ is inside the _src_ directory and could be compiled and executed in two different ways. 

#### Building an executable jar file

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/Concatenate.git
```
On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (6 or higher) installed, a Java executable jar file could be created. In a command-line window, go to the _src_ directory and type:
```bash
javac Concatenate.java 
echo Main-Class: Concatenate > MANIFEST.MF 
jar -cmvf MANIFEST.MF Concatenate.jar Concatenate.class 
rm MANIFEST.MF Concatenate.class 
```
This will create the executable jar file `Concatenate.jar` that could be run with the following command line model:
```bash
java -jar Concatenate.jar [options]
```

#### Building a native code binary

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/Concatenate.git
```
On computers with [GraalVM](hhttps://www.graalvm.org/downloads/) installed, a native executable can be built. In a command-line window, go to the _src_ directory, and type:
```bash
javac Concatenate.java 
native-image Concatenate Concatenate
rm Concatenate.class
```
This will create the native executable `Conctenate` that can be run with the following command line model:
```bash
./Concatenate [options]
```


## Usage

Launch _Concatenate_ without option to read the following documentation:

```
 Concatenate

 USAGE:   Concatenate  [-i <infile>]  [-o <outfile>]  [-f]  [-l <integer>]

 where options are:

 -i <infile>   to indicate the input file that contains containing every MSA file name (one
               per line). If the  character % is set before a file name,  the corresponding
               MSA will be not used to  build the supermatrix of characters.  Each MSA file
               should contain one MSA  (DNA, RNA,  amino acid, O1, ...)  in either FASTA or
               PHYLIP format. Relative/absolute path is allowed for each MSA file (default:
               datafiles.txt).
 -o <outfile>  To set the output file name (default: supermatrix.phy or supermatrix.fasta)
 -f            To write the supermatrix of characters in FASTA format (default: PHYLIP)
 -l <integer>  To set the maximum length of the written sequences (default: infinity); when 
               option -f is not set, this option leads to a PHYLIP-interleaved ouput file 

```

## Notes

When used with only one FASTA- or PHYLIP-formatted MSA file, _Concatenate_ could be used for converting it into a PHYLIP- or FASTA-formatted one, respectively.


